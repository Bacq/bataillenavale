package utiles;

import java.awt.image.BufferedImage;

public class RotationBufferedImage {
	// -90
	static public BufferedImage rotationN90(BufferedImage img) {
	    BufferedImage image = new BufferedImage(img.getHeight(), img.getWidth(), img.getType());
	    for(int x=0 ; x<img.getWidth() ; x++)
	        for(int y=0 ; y<img.getHeight() ; y++)
	            image.setRGB(y, x, img.getRGB(img.getWidth()-x-1, y));
	    return image;
	}
	 
	// 90
	static public BufferedImage rotation90(BufferedImage img) {
	    BufferedImage image = new BufferedImage(img.getHeight(), img.getWidth(), img.getType());
	    for(int x=0 ; x<img.getWidth() ; x++)
	        for(int y=0 ; y<img.getHeight() ; y++)
	            image.setRGB(y, x, img.getRGB(x, img.getHeight()-y-1));
	    return image;
	}
	 
	// 180
	static public BufferedImage rotation180(BufferedImage img) {
	    BufferedImage image = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
	    for(int x=0 ; x<img.getWidth() ; x++)
	        for(int y=0 ; y<img.getHeight() ; y++)
	            image.setRGB(x, y, img.getRGB(img.getWidth()-x-1, img.getHeight()-y-1));
	    return image;
	}
	
	static public BufferedImage rotationSelonDirection(BufferedImage img, int[] direction){
		switch(direction[0]){
		case -1:
			//haut
			return RotationBufferedImage.rotationN90(img);
		case 0:
			switch(direction[1]){
			case -1:
				//gauche
				return RotationBufferedImage.rotation180(img);
			case 1:
				//droite
				return img;
			}
			break;
		case 1:
			//bas
			return RotationBufferedImage.rotation90(img);
		}
		
		return img;
	}
}
