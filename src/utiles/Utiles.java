package utiles;

public class Utiles {
	/**
	 * Retourne un entier aléatoire entre min et max
	 * @param min comprit
	 * @param max non comprit
	 * @return
	 */	
	public static int random(int min, int max){
		return (int) (min + Math.random() * (max - min));
	}
}
