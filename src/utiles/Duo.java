package utiles;

public class Duo<T, S> { 
	  //Variable d'instance de type T
	  private T valeur1;

	  //Variable d'instance de type S
	  private S valeur2;
	        
	  //Constructeur par défaut
	  public Duo(){
	    this.valeur1 = null;
	    this.valeur2 = null;
	  }        

	  //Constructeur avec paramètres
	  public Duo(T val1, S val2){
	    this.valeur1 = val1;
	    this.valeur2 = val2;
	  }
	        
	  //Méthodes d'initialisation des deux valeurs
	  public void setValeur(T val1, S val2){
	    this.valeur1 = val1;
	    this.valeur2 = val2;
	  }
	 
	  //Retourne la valeur T
	  public T getValeur1() {
	    return valeur1;
	  }
	 
	  //Définit la valeur T
	  public void setValeur1(T valeur1) {
	    this.valeur1 = valeur1;
	  }
	 
	  //Retourne la valeur S
	  public S getValeur2() {
	    return valeur2;
	  }
	 
	  //Définit la valeur S
	  public void setValeur2(S valeur2) {
	    this.valeur2 = valeur2;
	  }
	  
	  @Override
	  public boolean equals(Object obj) {
		  if (this == obj)
			  return true;
		
		  if (obj == null)
			  return false;
		
		  if (getClass() != obj.getClass())
				return false;
		
		  Duo other = (Duo) obj;
		  if (valeur1 == null) {
			  if (other.valeur1 != null){
				  return false;
			  }
		  }
		  else{
			  if (!valeur1.equals(other.valeur1)){
				  return false;
			  }
			  if (valeur2 == null) {
				  if(other.valeur2 != null){
					  return false;
				  }
			  }
				else{
					if (!valeur2.equals(other.valeur2)){
						return false;
					}
				}			
		  }
		  return true;
	  }
	  
	  
	  
	}
