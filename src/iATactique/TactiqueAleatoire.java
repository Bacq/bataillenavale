package iATactique;

import java.util.ArrayList;

import utiles.Duo;

import model.cases.decorateurCases.CaseVise;

import model.plateaux.Plateau;

public class TactiqueAleatoire implements AbstractTactique{

	private ArrayList<Duo<Integer, Integer>> listeCaseDejaVise;
	
	public TactiqueAleatoire() {
		this.listeCaseDejaVise = new ArrayList<Duo<Integer, Integer>>();
	}
	
	public TactiqueAleatoire(Plateau plateauTir){
		this.listeCaseDejaVise = new ArrayList<Duo<Integer, Integer>>();
		this.remplirTactique(plateauTir);
	}

	
	public Duo<Integer, Integer> tir() {
		Duo<Integer, Integer> caseVise;
		
		do{
			int ligTir = (int) (Math.random() * Plateau.getNB_LIG());
			int colTir = (int) (Math.random() * Plateau.getNB_COL());
			caseVise = new Duo<>(ligTir, colTir);
		}while(this.listeCaseDejaVise.contains(caseVise)) ;
		
		//Ajoute la case au case sur lesquelles on a déjà tirer
		listeCaseDejaVise.add(caseVise);
		
		return caseVise;
	}

	@Override
	public void remplirTactique(Plateau plateauTir) {
		for(int lig=0; lig<Plateau.getNB_LIG(); lig++){
			for(int col=0; col<Plateau.getNB_COL(); col++){
				if(plateauTir.getCase(lig, col) instanceof CaseVise){
					this.listeCaseDejaVise.add(new Duo<>(lig, col));
				}
			}
		}
	}

	@Override
	public String toCSV() {
		return("Aleatoire,");
	}

}