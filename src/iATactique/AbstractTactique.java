package iATactique;

import model.plateaux.Plateau;
import utiles.Duo;

public interface AbstractTactique {
		
	public abstract Duo<Integer, Integer> tir();
	
	public abstract void remplirTactique(Plateau plateauTir);

	public abstract Object toCSV();

}