package iATactique;

import java.util.ArrayList;

import utiles.Duo;

import model.cases.decorateurCases.CaseVise;
import model.plateaux.Plateau;

public class TactiqueCroix implements AbstractTactique{

	private ArrayList<Duo<Integer, Integer>> listeCaseDejaVise;
	private ArrayList<Duo<Integer, Integer>> listeATire;
	
	public TactiqueCroix() {
		this.listeCaseDejaVise = new ArrayList<Duo<Integer, Integer>>();
		this.listeATire = new ArrayList<Duo<Integer, Integer>>();
	}
	
	public TactiqueCroix(Plateau plateauTir) {
		this.listeCaseDejaVise = new ArrayList<Duo<Integer, Integer>>();
		this.listeATire = new ArrayList<Duo<Integer, Integer>>();
		this.remplirTactique(plateauTir);
	}

	public Duo<Integer, Integer> tir() {
		Duo<Integer, Integer> caseVise;
		
		if(listeATire.size() == 0){
			do{
				int ligTir = (int) (Math.random() * Plateau.getNB_LIG());
				int colTir = (int) (Math.random() * Plateau.getNB_COL());
				caseVise = new Duo<>(ligTir, colTir);
			}while(this.listeCaseDejaVise.contains(caseVise)) ;
			
			this.listeCaseDejaVise.add(caseVise);
			
			for(int i=-1;i<2;i++) {
				for(int j=-1;j<2;j++) {
					if(Math.abs(i)+Math.abs(j)!=0 && (j==0||i==0) ){
						if(!this.listeCaseDejaVise.contains(new Duo(i,j)) && caseVise.getValeur1() + i >= 0 && caseVise.getValeur1() + i < Plateau.getNB_LIG() && caseVise.getValeur2() + j >= 0 && caseVise.getValeur2() + j < Plateau.getNB_COL()){
							System.out.println("BATEAU TOUCHE");
							this.listeATire.add(new Duo<Integer, Integer>(caseVise.getValeur1() + i, caseVise.getValeur2() + j));
						}
					}

				}
			}
		}
		else {
			caseVise = this.listeATire.get(0);

			this.listeCaseDejaVise.add(this.listeATire.get(0));
			this.listeATire.remove(0);
		}
		
		return caseVise;
	}

	@Override
	public void remplirTactique(Plateau plateauTir) {
		for(int lig=0; lig<Plateau.getNB_LIG(); lig++){
			for(int col=0; col<Plateau.getNB_COL(); col++){
				if(plateauTir.getCase(lig, col) instanceof CaseVise){
					this.listeCaseDejaVise.add(new Duo<>(lig, col));
				}
			}
		}
	}
	
	@Override
	public String toCSV() {
		return("Croix,");
	}

}