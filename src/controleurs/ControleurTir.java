package controleurs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.BatailleNavale;
import model.plateaux.Plateau;

public class ControleurTir implements ActionListener{
	
	private BatailleNavale model;
	private int lig;
	private int col;
		
	public ControleurTir(BatailleNavale aModel, int aLig, int aCol){
		this. model = aModel;
		this.lig = aLig;
		this.col = aCol;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		Plateau plateauTir = this.model.getPlateauTirJoueur();
		if(plateauTir.getCase(lig, col).isClickable() && this.model.isGameRunning()){
			if(this.model.isModeMunitions()){
				if(this.model.peutTirerModeMunitions()){
					this.model.joueurTir(col, lig);
				}
			}
			else{
				this.model.joueurTir(col, lig);
			}			
		}
	}	
	
}
