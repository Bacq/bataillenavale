package controleurs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import utiles.Duo;

import model.BatailleNavale;

public class ControleurPlacementBateau implements ActionListener{
	
	private BatailleNavale model;
	private int lig;
	private int col;
	
	private static Duo<Integer, Integer> case1;
	
	public ControleurPlacementBateau(BatailleNavale aModel, int aLig, int aCol){
		this. model = aModel;
		this.lig = aLig;
		this.col = aCol;
	}
	
	public void placementBateau(int lig1, int col1){
		//Si on a pas encore cliquer sur une case
		if(case1 == null){
			case1 = new Duo<>(lig1, col1);
		}
		//Si on deja cliquer sur une case
		else{
			this.model.getPlateauJoueurJoueur().placementBateau(case1.getValeur1(), case1.getValeur2(), lig, col);
			this.model.update();
			//On remet case1 un null une fois qu'on a essayer de place le bateau
			case1 = null;
		}
	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(this.model.isPlacementBateau()){
			this.placementBateau(lig, col);
		}
		if(this.model.isGameRunning()){
			this.model.selectionBateau(lig,col);
		}

	}	
	
}
