package model.joueurs;

import model.fabriques.chantiers.Chantier;
import model.plateaux.Plateau;
import model.plateaux.PlateauJoueur;
import model.plateaux.PlateauTir;

public class Joueur {

	private Plateau plateauJoueur;
	private Plateau plateauTir;
	
	public Joueur(Chantier chantier){
		this.plateauJoueur = new PlateauJoueur(chantier);
		this.plateauTir = new PlateauTir(chantier); 
	}
	
	public Plateau getPlateauJoueur(){
		return this.plateauJoueur;
	}
	
	public Plateau getPlateauTir(){
		return this.plateauTir;
	}
	
	public void setChantier(Chantier chantier){
		this.plateauJoueur.setChantier(chantier);
		this.plateauTir.setChantier(chantier);
	}
	
	public String toCSV() {
		StringBuilder sbCSV = new StringBuilder();
		sbCSV.append(this.plateauJoueur.toCSV());
//		sbCSV.append(this.plateauTir.toCSV());
		return sbCSV.toString();
	}
	
}
