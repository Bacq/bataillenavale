package model.joueurs;

import model.fabriques.chantiers.Chantier;
import utiles.Duo;

import iATactique.AbstractTactique;
import iATactique.TactiqueAleatoire;

public class IA extends Joueur{

	private AbstractTactique tactique;
	
	public IA(Chantier chantier){
		super(chantier);
		this.tactique = new TactiqueAleatoire();
	}
	
	public Duo<Integer, Integer> iaTire(){
		return this.tactique.tir();
	}

	public void setTactique(AbstractTactique ia) {
		tactique = ia;
	}

	public AbstractTactique getTactique() {
		return tactique;
	}
	
}
