package model.etats;

public enum EtatBatailleNavale {
	END,
	PLACEMENT,
	RUNNING,
	CONFIGURATION;
}
