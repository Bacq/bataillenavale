package model.bateaux;

public class Bateau3Cases extends Bateau {

	public Bateau3Cases(int aPointVie, int aNbProjectiles, String nom) {
		super(aPointVie, aNbProjectiles, 3, nom);
	}

}
