package model.bateaux;

import java.util.ArrayList;

public abstract class Bateau {

	protected int pointVie;
	protected int nbProjectiles;
	protected int nbCases;
	protected String  nom;
	protected ArrayList<int[]> coordonnees;

	public static int NB_CASES_MAX = 4;
	public static int NB_CASES_MIN = 2;
	
	public final static int BATEAU2 = 2;
	public final static int BATEAU3 = 3;
	public final static int BATEAU4 = 4;

	
	public Bateau(int aPointVie, int aNbProjectiles, int aNbCases, String aNom){
		coordonnees = new ArrayList<int[]>();
		this.pointVie = aPointVie;
		this.nbProjectiles = aNbProjectiles;
		this.nbCases = aNbCases;
		this.nom = aNom;
	}
	
	public int getNbCases(){
		return this.nbCases;
	}
	
	public String toCSV() {
		StringBuilder sbCSV = new StringBuilder();
		sbCSV.append(pointVie + "," + nbProjectiles + ","  + nbCases + "," + nom);
		for (int[] coord : coordonnees) {
			sbCSV.append(";" + coord[0] + "," + coord[1]);
		}
		sbCSV.append("\n");
		return(sbCSV.toString());
	}
	
	public void addCase(int lig, int col) {
		this.coordonnees.add(new int[] {lig,col});
	}
	
	public void degat(){
		this.pointVie--;
	}
	
	public boolean isDetruit(){
		return this.getPointVie() <= 0;
	}
	
	public int getPointVie(){
		return this.pointVie;
	}

	public int getProjectile() {
		return this.nbProjectiles;
	}

	public void decrementerMunition() {
		this.nbProjectiles --;		
	}
	
	public void setPointVie(int pv){
		this.pointVie = pv;
	}
	
	public void setNbProjectiles(int np){
		this.nbProjectiles = np;
	}
	
	public ArrayList<int[]> getListeCases(){
		return this.coordonnees;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((coordonnees == null) ? 0 : coordonnees.hashCode());
		result = prime * result + nbCases;
		result = prime * result + nbProjectiles;
		result = prime * result + pointVie;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		Bateau other = (Bateau) obj;
		if (coordonnees == null) {
			if (other.coordonnees != null)
				return false;
		}else
			if (!coordonnees.equals(other.coordonnees))
				return false;
		
		if (nbCases != other.nbCases)
			return false;
		
		if (nbProjectiles != other.nbProjectiles)
			return false;
		
		if (pointVie != other.pointVie)
			return false;
		
		return true;
	}
	
}
