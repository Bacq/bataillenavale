package model.bateaux.bateaux17e;

import model.bateaux.Bateau2Cases;

public class Bateau2Case17e extends Bateau2Cases {

	private static String NOM = "Corvette";
	private static int NB_PROJECTILES = 4;
	private static int POINTS_VIE = 1;
	
	public Bateau2Case17e() {
		super(POINTS_VIE, NB_PROJECTILES, NOM);
	}

}
