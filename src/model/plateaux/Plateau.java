package model.plateaux;

import java.util.ArrayList;

import utiles.Utiles;
import model.bateaux.Bateau;
import model.cases.Case;
import model.cases.CaseConcrete;
import model.cases.decorateurCases.CaseBateau;
import model.fabriques.FabriqueImage;
import model.fabriques.chantiers.Chantier;

public abstract class Plateau {

	protected Case[][] plateau;
	protected ArrayList<Bateau> bateaux;
	protected Chantier chantierNavale;
	
	private static int LIG_DEFAUT = 10;
	private static int COL_DEFAUT = 10;
	private static int NB_COL;
	private static int NB_LIG;
	
	private static int NB_BATEAUX_2_CASES_MAX = 3;
	private static int NB_BATEAUX_3_CASES_MAX = 2;
	private static int NB_BATEAUX_4_CASES_MAX = 1;
	
	private final static int DIRECTION_NORD = 0;
	private final static int DIRECTION_SUD = 1;
	private final static int DIRECTION_EST = 2;
	private final static int DIRECTION_OUEST = 3;

	private static int coeffDirectionLig = 0;
	private static int coeffDirectionCol = 1;
	
	public Plateau(Chantier chantier){
		NB_LIG = getLIG_DEFAUT();
		NB_COL = getCOL_DEFAUT();
		
		this.chantierNavale = chantier;
		this.bateaux = new ArrayList<Bateau>();

		this.plateau = new Case[NB_LIG][NB_COL];
		for(int lig=0; lig<NB_LIG; lig++){
			for(int col=0; col<NB_COL; col++){
				this.plateau[lig][col] = new CaseConcrete();
			}
		}		
	}
	
	public void setChantier(Chantier chantier){
		this.chantierNavale = chantier;
	}
	
	
	/************************************* GESTION LIG / COL *************************************/

	
	public static int getCOL_DEFAUT(){
		return COL_DEFAUT;
	}
	
	public static int getLIG_DEFAUT(){
		return LIG_DEFAUT;
	}
	
	public static int getNB_COL(){
		return NB_COL;
	}
	
	public static int getNB_LIG(){
		return NB_LIG;
	}
	
	/************************************* GESTION CASE *************************************/

	public Case getCase(int lig, int col){
		return this.plateau[lig][col];
	}
	
	public void setCase(Case c, int lig, int col){
		this.plateau[lig][col] = c;
	}
	
	/************************************* GESTION BATEAUX *************************************/
	
	public void ajouterBateau(Bateau bateau){
		this.bateaux.add(bateau);
	}

	public void detruitBateau(Bateau bateau){
		this.bateaux.remove(bateau);
	}

	public int getNbMunitions(){
		int nbProjectile = 0;
		for(Bateau b :bateaux){
			if(!b.isDetruit()){
				nbProjectile += b.getProjectile();
			}
		}
		return nbProjectile;
	}
	
	public void tirerBateauRandom(){
		Bateau b;
		do{
			int rand = Utiles.random(0, bateaux.size()-1);
			b = this.bateaux.get(rand);
		}
		while(b.getProjectile()==0);
		b.decrementerMunition();
	}
	
	public void placementRandomBateau() {
		for(int i=Bateau.NB_CASES_MIN; i <= Bateau.NB_CASES_MAX; i++) {
			while(nbBateauCorrect(i)) {
				//(lig1, col1) premiere case du bateau
				int lig1 = (int) (Math.random() * Plateau.getNB_LIG());
				int col1 = (int) (Math.random() * Plateau.getNB_COL());
				
				int[] direction = this.getDirectionAlea();
				//(lig2, col2) = dernière case du bateau
				int lig2 = lig1 + direction[0] * (i-1);
				int col2 = col1 + direction[1] * (i-1);
				//Place le bateau si pas de problèmes (chevauchement de bateaux, etc...)
				placementBateau(lig1, col1, lig2, col2);
			}
		}
	}
	
	public int[] getDirectionAlea(){
		int randDirection = (int) (Math.random()*4);
		int[] direction = new int[2];
		switch(randDirection) {
		 	case DIRECTION_NORD:
		    	direction[0] =-1;
		    	direction[1] = 0;
		    	break;
		    case DIRECTION_SUD:
		    	direction[0] =1;
		    	direction[1] = 0;
		    	break;
		    case DIRECTION_EST:
		    	direction[0] = 0;
		    	direction[1] = -1;
		    	break;
		    case DIRECTION_OUEST:
		    	direction[0] = 0;
		    	direction[1] = 1;
		    	break;
		    
		    }
		return direction;
	}
		
	/**
	 * Test si on peut poser un bateau de la case1(lig1, col1) à la case2(lig2, col2)
	 * Si oui le pose
	 * @param lig1
	 * @param col1
	 * @param lig2
	 * @param col2
	 */
	public void placementBateau(int lig1, int col1, int lig2, int col2){
		//Case1 lig1 col1
		//Case2 lig2 col2
		int deltaLig = lig1 - lig2;
		int deltaCol = col1 - col2;
		int absDeltaLig = Math.abs(deltaLig);
		int absDeltaCol = Math.abs(deltaCol);
		
		if(this.distanceCorrecte(absDeltaLig, absDeltaCol)){
			//distance = nbCases du bateaux
			//+ 1, car la delta montre la différence, il manquera donc une case
			int distance = absDeltaLig + absDeltaCol + 1;
			int[] coeffsDirection = this.getCoeffsDirection(deltaLig, deltaCol, absDeltaLig, absDeltaCol);
			
			if(this.placementPossible(distance, lig1, col1, coeffsDirection) && this.nbBateauCorrect(distance)){
				this.placementBateauDansPlateau(distance, lig1, col1, coeffsDirection);
			}			
		}
	}
	
	
	/**
	 * @param taille
	 * @return vrai si on peu encore mettre des bateaux de taille cases
	 */
	public boolean nbBateauCorrect(int taille){
		//+1 pour le bateau que on va ajouter
		return (this.getNbBateaux(taille) + 1) <= this.getnbBateauMax(taille);
	}
	
	/**
	 * @param taille
	 * @return le nb max de bateau de taille case
	 */
	public int getnbBateauMax(int taille){
		int nbBateauMax = 0;
		switch(taille){
		case Bateau.BATEAU2:
			nbBateauMax = Plateau.NB_BATEAUX_2_CASES_MAX;
			break;
		case Bateau.BATEAU3:
			nbBateauMax = Plateau.NB_BATEAUX_3_CASES_MAX;
			break;
		case Bateau.BATEAU4:
			nbBateauMax = Plateau.NB_BATEAUX_4_CASES_MAX;
			break;
		}		
		
		return nbBateauMax;
	}
	
	/**
	 * @param distance
	 * @param lig1
	 * @param col1
	 * @param coeffsDirection
	 * @return vrai si il n'y a pas de bateau sur le "chemin" du bateau que on est en train de construire
	 */
	public boolean placementPossible(int distance, int lig1, int col1, int[]coeffsDirection){
		boolean retour = true;
		for(int i=0; i<distance; i++){
			int newLig = lig1 + i*coeffsDirection[coeffDirectionLig];
			int newCol = col1 + i*coeffsDirection[coeffDirectionCol];
			//Si la case est bien dans le plateau
			if(newLig < 0 || newLig >= Plateau.NB_LIG || newCol < 0 || newCol >= Plateau.NB_COL){
				retour = false;
			}
			else{
				Case ancienneCase = this.getCase(newLig, newCol);
				//Si la case courante est un bateau
				if(ancienneCase.touche()){
					retour = false;
				}
			}
		}
		return retour;
	}
	
	
	/**
	 * Place les CaseBateau, et ajoute le bateau au plateau
	 * @param distance
	 * @param lig1
	 * @param col1
	 * @param coeffsDirection
	 */
	public void placementBateauDansPlateau(int distance, int lig1, int col1, int[]coeffsDirection){
		Bateau bateau = this.chantierNavale.getBateauSelonTaille(distance);
		for(int i=0; i<distance; i++){
			//Cree la CaseBateau
			Case ancienneCase = this.getCase(lig1 + i*coeffsDirection[coeffDirectionLig], col1 + i*coeffsDirection[coeffDirectionCol]);
			
			//On ajoute les coordonnees de la case au bateau
			bateau.addCase(lig1 + i*coeffsDirection[coeffDirectionLig], col1 + i*coeffsDirection[coeffDirectionCol]);

			CaseBateau caseBateau = new CaseBateau(ancienneCase, bateau, FabriqueImage.getImageBateau(i,distance, coeffsDirection, chantierNavale.toString()));
	
			//Set la CaseBateau dans le plateau à lig, col
			this.setCase(caseBateau, lig1 + i*coeffsDirection[coeffDirectionLig], col1 + i*coeffsDirection[coeffDirectionCol]);			
		}
		//On ajoute le bateau au plateau (dans les deux liste)
		this.ajouterBateau(bateau);
	}
	
	
	/**
	 * @param deltaLig
	 * @param deltaCol
	 * @param absDeltaLig
	 * @param absDeltaCol
	 * @return un coefficient de direction (haut, bas, gauche ou droite) pour aller de la case1 à la case2
	 */
	public int[] getCoeffsDirection(int deltaLig, int deltaCol, int absDeltaLig, int absDeltaCol){
		int[] coeffsDirection = new int[2];
		//Si c'est une direction Verticale
		if(deltaLig != 0){
			//Si deltaLig est positif on doit aller vers le haut, donc on multiplie par -absDeltaLig pour que le coeff soit -1
			//Si deltaLig est negatif on doit descendre, donc on multiplie par -absDeltaLig pour que le coeff soit 1
			coeffsDirection[coeffDirectionLig] = deltaLig / (-absDeltaLig);
			coeffsDirection[coeffDirectionCol] = 0;			
		}
		//Sinon c'est une direction Horizontale
		else{
			//Si deltaCol est positif on doit aller vers la gauche, donc on multiplie par -absDeltaCol pour que le coeff soit -1
			//Si deltaCol est negatif on doit aller vers la drotie, donc on multiplie par -absDeltaCol pour que le coeff soit 1
			coeffsDirection[coeffDirectionLig] = 0;
			coeffsDirection[coeffDirectionCol] = deltaCol / (-absDeltaCol);
		}
		return coeffsDirection;		
	}
	
	
	/**
	 * @param deltaLig
	 * @param deltaCol
	 * @return vrai si la bateau contient un nombre de case correct (entre NB_CASES_MIN et NB_CASES_MAX)
	 */
	public boolean distanceCorrecte(int absDeltaLig, int absDeltaCol){
		//Si un absDelta est > 0 et l'autre = 0
		//pour avoir des uniquement une direction horizontale ou verticales (et non en diagonale)
		if(!(absDeltaLig > 0 && absDeltaCol > 0) && !(absDeltaLig == 0 && absDeltaCol == 0) ){ 
			//+ 1, car la delta montre la différence, il manquera donc une case
			return ((absDeltaLig + absDeltaCol + 1 <= Bateau.NB_CASES_MAX) && (absDeltaLig + absDeltaCol + 1 >= Bateau.NB_CASES_MIN));
		}
		return false;
	}

	/**
	 * retourn le nombre de bateau selon la taille
	 * @param taille
	 * @return
	 */
	public int getNbBateaux(int taille){
		int nbBateaux = 0;
		for(Bateau bateau: this.bateaux){
			if(bateau.getNbCases() == taille && (!bateau.isDetruit())){
				nbBateaux++;
			}
		}
		return nbBateaux;
	}
	
	/**
	 * retourn le nombre de bateau total
	 * @return
	 */
	public int getNbBateaux(){
		int nbBateaux = 0;
		for(Bateau bateau: this.bateaux){
			if((!bateau.isDetruit())){
				nbBateaux++;
			}
		}
		return nbBateaux;
	}
	
	
	
	public void setNbrMunitionDernierBateau(int munitions){
		bateaux.get(bateaux.size()-1).setNbProjectiles(munitions);
	}


	/************************************* ABSTRACT METHOD *************************************/
	
	public abstract void selectionCase(int lig, int col);
	
	/************************************* GESTION CSV *****************************************/
	
	public String toCSV() {
		StringBuilder sbCSV = new StringBuilder();
		sbCSV.append(this.bateaux.size() + "\n");
		for (Bateau b : this.bateaux) {
			sbCSV.append(b.toCSV());
		}
		for (Case cases[] : this.plateau) {
			for (Case c : cases) {
				sbCSV.append(c.toCSV()+",");
			}
			sbCSV.append("\n");
		}
				
		return sbCSV.toString();
	}
}
