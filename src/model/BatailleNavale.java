package model;
import iATactique.AbstractTactique;

import iATactique.TactiqueAleatoire;
import iATactique.TactiqueCroix;

import java.util.Observable;

import utiles.Duo;

import model.bateaux.Bateau;
import model.cases.Case;
import model.cases.decorateurCases.CaseBateau;
import model.cases.decorateurCases.CaseSelectionnee;
import model.cases.decorateurCases.CaseVise;
import model.etats.EtatBatailleNavale;
import model.etats.ModeBatailleNavale;
import model.fabriques.FabriqueImage;
import model.fabriques.chantiers.Chantier;
import model.fabriques.chantiers.Chantier17e;
import model.fabriques.chantiers.Chantier20e;
import model.joueurs.IA;
import model.joueurs.Joueur;
import model.plateaux.Plateau;


public class BatailleNavale extends Observable{

	private Joueur joueur;
	private IA ordinateur;
	private String gagne;

	private ModeBatailleNavale mode;
	private EtatBatailleNavale etat;
	
	private Bateau bateauSelectionne;
	
	private Chantier chantierNavale;
		
	public BatailleNavale(){
		this.init();
	}
	
	public void BatailleNavale(String state, String chantier, String mode){
		setEtat(state);
		setChantier(chantier);
		setMode(mode);
		this.joueur = new Joueur(this.chantierNavale);
		this.ordinateur = new IA(this.chantierNavale);
		update();
	}
	
	public void init(){
		this.mode = ModeBatailleNavale.NORMAL;		
		this.etat = EtatBatailleNavale.CONFIGURATION;
		this.chantierNavale = new Chantier17e();

		this.joueur = new Joueur(chantierNavale);
		this.ordinateur = new IA(chantierNavale);

		this.bateauSelectionne = null;
	}
	
	public void update(){
		setChanged();
		notifyObservers();
		clearChanged();	
	}
	
	public void chargerBatailleNavale(String state, String chantier, String mode){
		setEtat(state);
		setChantier(chantier);
		setMode(mode);
		this.joueur = new Joueur(this.chantierNavale);
		this.ordinateur = new IA(this.chantierNavale);
		update();
	}

	/************************************* GESTION ETAT *************************************/
	
	public boolean isGameRunning(){
		return this.etat == EtatBatailleNavale.RUNNING;
	}
	
	public boolean isPlacementBateau(){
		return this.etat == EtatBatailleNavale.PLACEMENT;
	}
	
	public boolean isConfiguration(){
		return this.etat == EtatBatailleNavale.CONFIGURATION;
	}
	
	public boolean isEnd(){
		return this.etat == EtatBatailleNavale.END;
	}
	
	public void setEtatRunning(){
		this.etat = EtatBatailleNavale.RUNNING;
	}
	
	public void setEtatEnd(){
		this.etat = EtatBatailleNavale.END;
	}
	
	public void setEtatPlacement(){
		this.etat = EtatBatailleNavale.PLACEMENT;
		this.update();
	}
	
	public void lancerPartie(){
		this.setEtatRunning();
		this.update();
	}
	
	/************************************* GESTION MODE *************************************/

	
	public boolean isModeNormal(){
		return this.mode == ModeBatailleNavale.NORMAL;
	}
	
	public boolean isModeMunitions(){
		return this.mode == ModeBatailleNavale.MUNITIONS;
	}
	
	public void setModeNormal(){
		this.mode = ModeBatailleNavale.NORMAL;
	}
	
	public void setModeMunitions(){
		this.mode = ModeBatailleNavale.MUNITIONS;
	}
	
	public ModeBatailleNavale getMode(){
		return this.mode;
	}
		

	public void chargerPartie(String etat, String chantier, String mode) {
		BatailleNavale(etat, chantier, mode);
	}
	
	public void setEtat(String state) {
		if(state.equals("Running")) {
			etat = EtatBatailleNavale.RUNNING;
		}else if (state.equals("Placement")){
			etat = EtatBatailleNavale.PLACEMENT;
		}else {
			etat = EtatBatailleNavale.CONFIGURATION;
		}
	}
	
	public void setMode(String mode) {
		if (mode.equals("Normal")){
			this.mode = ModeBatailleNavale.NORMAL;
		}else{
			this.mode = ModeBatailleNavale.MUNITIONS;
		}
	}
	
	
	/************************************* GESTION PLATEAU *************************************/
	
	public Plateau getPlateauJoueurJoueur(){
		return this.getJoueur().getPlateauJoueur();
	}
	
	public void setCasePlateauJoueurJoueur(Case caseAChanger, int lig, int col){
		this.getPlateauJoueurJoueur().setCase(caseAChanger, lig, col);
	}
	
	public Plateau getPlateauTirJoueur() {
		return this.getJoueur().getPlateauTir();
	}
	
	public Plateau getPlateauJoueurOrdinateur(){
		return this.getOrdinateur().getPlateauJoueur();
	}
	
	public void setCasePlateauJoueurOrdinateur(Case caseAChanger, int lig, int col){
		this.getPlateauJoueurOrdinateur().setCase(caseAChanger, lig, col);
	}
	

	public Plateau getPlateauTirOrdinateur() {
		return this.getOrdinateur().getPlateauTir();
	}
	
	/************************************* GESTION JOUEUR *************************************/
	
	public String getGagner(){
		return this.gagne;
	}
	
	/**
	 * Le joueur tir sur une case de l'ordinateur
	 * @param col
	 * @param lig
	 */
	public void joueurTir(int col, int lig){		
		Plateau plateauTirJoueur = this.getPlateauTirJoueur();
		Plateau plateauJoueurOrdinateur = this.getPlateauJoueurOrdinateur();	
		//Tir sur la case de l'ordinateur
		this.CaseVise(plateauTirJoueur, plateauJoueurOrdinateur, lig, col);
		
		//Si on est en mode munitions
		if(this.isModeMunitions()){
			//Reduit les munitions du bateau selectionné
			this.bateauSelectionne.decrementerMunition();
			//désélectionne le bateau
			this.deselectionBateau();
		}

		//L'ordinateur tir
		this.ordinateurTir();
		
		//Test de fin de partie
		this.setEtatActuel();
				
		//On update les vues
		this.update();
	}
	
	public void setEtatActuel(){
		if(this.isModeNormal()){
			this.setEtatActuelModeNormal();
		}
		
		if(this.isModeMunitions()){
			this.setEtatActuelModeMunitions();
		}
	}
	
	public void setEtatActuelModeNormal(){
		if(this.getPlateauJoueurJoueur().getNbBateaux() <= 0){
			this.setEtatEnd();
			this.gagne = "perdu";
		}
		
		if(this.getPlateauJoueurOrdinateur().getNbBateaux() <= 0){
			this.setEtatEnd();
			this.gagne = "gagner";
		}
	}
	
	public void setEtatActuelModeMunitions(){
		//Si on a plus de munition
		if(this.getPlateauJoueurJoueur().getNbMunitions() <= 0){
			//On mets l'etat a end
			this.setEtatEnd();
			//Si on a plus de bateau que l'ordi
			if(this.getPlateauJoueurJoueur().getNbBateaux() > this.getPlateauJoueurOrdinateur().getNbBateaux()){
				this.gagne = "gagner";
			}
			else{
				if(this.getPlateauJoueurJoueur().getNbBateaux() < this.getPlateauJoueurOrdinateur().getNbBateaux()){
					this.gagne = "perdu";
				}
				else{
					this.gagne = "egalité";
				}
			}
		}
		
		if(this.getPlateauJoueurJoueur().getNbBateaux() <= 0){
			this.setEtatEnd();
			this.gagne = "perdu";
		}
		
		if(this.getPlateauJoueurOrdinateur().getNbBateaux() <= 0){
			this.setEtatEnd();
			this.gagne = "gagner";
		}
	}
	


	/**
	 * L'ordinateur tir sur une case du joueur
	 */
	public void ordinateurTir(){	
		//Recupere la case sur laquelle l'ia va tirer
		Duo<Integer, Integer> caseVise = this.ordinateur.iaTire();
		
		//Tir sur la case du joueur, caseVise.getValeur1() = lig, caseVise.getValeur2() = col
		this.CaseVise(this.getPlateauTirOrdinateur(), this.getPlateauJoueurJoueur(), caseVise.getValeur1(), caseVise.getValeur2());
	}
	
	/**
	 * Tir sur une case
	 * @param plateauTir
	 * @param plateauJoueur
	 * @param lig
		// TODO Auto-generated method stub
	 * @param col
	 */
	public void CaseVise(Plateau plateauTir, Plateau plateauJoueur, int lig, int col){
		Case caseVisePlateauTir, caseVisePlateauJoueur;

		//Si la case est un bateau dans le plateau adversaire
		if(plateauJoueur.getCase(lig, col).touche()){
			//Retire le bateau si il est detruit
			this.degatBateauPlateauJoueur(plateauTir, plateauJoueur, lig, col);

			caseVisePlateauTir = new CaseVise(plateauTir.getCase(lig, col), FabriqueImage.getImageTouche());
			caseVisePlateauJoueur = new CaseVise(plateauJoueur.getCase(lig, col), FabriqueImage.getImageTouche());
		}
		//Sinon le tire touche de l'eau
		else{
			caseVisePlateauTir = new CaseVise(plateauTir.getCase(lig, col), FabriqueImage.getImageRate());
			caseVisePlateauJoueur = new CaseVise(plateauJoueur.getCase(lig, col), FabriqueImage.getImageRate());
		}
		//Ajoute le decorateur dans le plateau de tir du joueur courant
		plateauTir.setCase(caseVisePlateauTir, lig, col);
		//Ajoute le decorateur dans le plateauJoueur adversaire
		plateauJoueur.setCase(caseVisePlateauJoueur, lig, col);
	}
	
	
	/**
	 * Retire le bateau de la liste du plateauJoueur
	 * si celu ci n'a plus de vie (si toute ses cases on étaient touchées)
	 * @param plateauJoueur
	 * @param lig
	 * @param col
	 */
	public void degatBateauPlateauJoueur(Plateau plateauTir, Plateau plateauJoueur, int lig, int col){
		CaseBateau caseBateau = (CaseBateau)(plateauJoueur.getCase(lig, col));
		//Enleve un point de vie au bateau
		caseBateau.degat();
		
		if(caseBateau.isDetruit()){
			for(int[] casesDuBateaux: caseBateau.getBateau().getListeCases()){
				if(plateauJoueur.getCase(casesDuBateaux[0], casesDuBateaux[1]).touche()){
					CaseVise caseDuBateauTir = new CaseVise(plateauTir.getCase(casesDuBateaux[0], casesDuBateaux[1]), FabriqueImage.getImageTouche());
					plateauTir.setCase(caseDuBateauTir, casesDuBateaux[0], casesDuBateaux[1]);
					
					CaseVise caseDuBateauJoueur = new CaseVise(plateauJoueur.getCase(casesDuBateaux[0], casesDuBateaux[1]), FabriqueImage.getImageTouche());
					plateauJoueur.setCase(caseDuBateauJoueur, casesDuBateaux[0], casesDuBateaux[1]);
				}
			}
		}
	}
	
	
	/**
	 * Deselectionne le bateauSelectionné
	 */
	public void deselectionBateau(){
		//Si un bateau est selectionné
		if(this.bateauSelectionne != null){
			//On enleve le decoreur de chaque case de l'ancien bateau selectionné
			for(int[] caseDuBateau: this.bateauSelectionne.getListeCases()){
				CaseSelectionnee caseDecore = ((CaseSelectionnee)this.getPlateauJoueurJoueur().getCase(caseDuBateau[0], caseDuBateau[1]));
				Case caseARemettre = caseDecore.getCaseDecore();
				this.setCasePlateauJoueurJoueur(caseARemettre, caseDuBateau[0], caseDuBateau[1]);
			}
			//Deselectionne le bateau
			this.bateauSelectionne = null;
		}
	}
	
	/**
	 * Selectionne un bateau, le decore avec CaseSelectionnée, si un bateau est deja selectionné, le deselectionne
	 * @param lig
	 * @param col
	 */
	public void selectionBateau(int lig, int col){
		if(this.isModeMunitions()){
			Case c = this.getPlateauJoueurJoueur().getCase(lig, col);
			//Si c'est bien un bateau
			if(c.touche()){
				this.selectionBateau(((CaseBateau)c));
			}
			else{
				if(c instanceof CaseVise){
					CaseVise caseVise = ((CaseVise)c);
					//Si la case touché est un bateau
					if(caseVise.getCaseDecore().touche()){
						this.selectionBateau(((CaseBateau)caseVise.getCaseDecore()));
					}
				}
			}
		}
		this.update();
	}
	
	public void selectionBateau(CaseBateau caseBateau){
		//Si le bateau n'est pas détruit
		if(!caseBateau.isDetruit()){
			
			//Deselectionne un bateau si il est selectionné
			this.deselectionBateau();

			this.bateauSelectionne = caseBateau.getBateau();
			
			//On decore chaque case du nouveau bateau selectionné
			Bateau bateau = caseBateau.getBateau();
			for(int[] caseDuBateau: bateau.getListeCases()){
				Case caseADecorer = this.getPlateauJoueurJoueur().getCase(caseDuBateau[0], caseDuBateau[1]);
				CaseSelectionnee caseDecore = new CaseSelectionnee(caseADecorer);
				this.setCasePlateauJoueurJoueur(caseDecore, caseDuBateau[0], caseDuBateau[1]);
			}					
		}
	}

	public boolean peutTirerModeMunitions(){
		return (this.bateauSelectionneNonNull() && this.bateauSelectionneAMunitions());
	}
	
	public boolean bateauSelectionneAMunitions(){
		return this.getMunitionsBateauSelectionne() > 0;
	}
	
	public int getMunitionsBateauSelectionne(){
		return this.bateauSelectionne.getProjectile();
	}
	
	public boolean bateauSelectionneNonNull(){
		return this.bateauSelectionne != null;
	}
	
	public void changeIa(AbstractTactique ia) {
		this.ordinateur.setTactique(ia);
	}

	public Joueur getJoueur(){
		return this.joueur;
	}
	
	public Joueur getOrdinateur(){
		return this.ordinateur;
	}
	
	public void chargerTactique(String tactique) {
		if(tactique.equals("Aleatoire")) {
			changeIa(new TactiqueAleatoire(ordinateur.getPlateauTir()));
		}else {
			changeIa(new TactiqueCroix(ordinateur.getPlateauTir()));
		}
	}
	
	/************************************* GESTION CHANTIER *************************************/
	
	public void setChantier(String chantier) {
		if(chantier.equals(Chantier.CHANTIER20E)) {
			this.chantierNavale = new Chantier20e();
		}
		else {
			this.chantierNavale = new Chantier17e();
		}
		
		this.joueur.setChantier(this.chantierNavale);
		this.ordinateur.setChantier(this.chantierNavale);
		this.getPlateauJoueurOrdinateur().placementRandomBateau();
	}
	
	/************************************* GESTION CSV ********************************************/
	
	public String getEtatToString() {
		if(isGameRunning()) {
			return "Running";
		}else if(isPlacementBateau()){
			return "Placement";
		}else {
			return "Configuration";
		}
	}
	
	public String getModeToString() {
		if(isModeNormal()) {
			return "Normal";
		}else {
			return "Munition";
		}
	}
	
	public String toCSV() {
		StringBuilder sbCSV = new StringBuilder();
		sbCSV.append(Plateau.getNB_LIG()+","+Plateau.getNB_COL()+","+getEtatToString()+",");
		sbCSV.append(this.chantierNavale.toCSV());
		sbCSV.append(ordinateur.getTactique().toCSV());
		sbCSV.append(getModeToString() + ",");
		sbCSV.append("\n");
		
		sbCSV.append("joueur1\n");
		sbCSV.append(this.joueur.toCSV());
		
		sbCSV.append("joueur2\n");
		sbCSV.append(this.ordinateur.toCSV());
		return sbCSV.toString();
	}

	
	
}
