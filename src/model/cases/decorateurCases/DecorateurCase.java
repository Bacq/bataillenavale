package model.cases.decorateurCases;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;

import model.cases.Case;

public abstract class DecorateurCase extends Case{
	protected Case caseDecore;
	
	public DecorateurCase(Case ancienneCase){
		this.caseDecore = ancienneCase;
	}
	
	
	/**
	 * @param image1
	 * @param image2
	 * @return une image, superposition de la deuxième sur la première
	 */
	public BufferedImage superposerImages(BufferedImage image1, BufferedImage image2){
		//Copie l'image2 pour ne pas la modifier
		
		Graphics2D g2d = image1.createGraphics();
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
		                RenderingHints.VALUE_ANTIALIAS_ON); 
		g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, 
		                RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY); 
	  
		g2d.drawImage(image2, 0, 0, null); 
		g2d.dispose(); 
	  
		return image1 ; 
	}
	
	/**
	 * copy une bufferedImage
	 * @param bi
	 * @return
	 */
	public BufferedImage deepCopy(BufferedImage bi) {
	    ColorModel cm = bi.getColorModel();
	    boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
	    WritableRaster raster = bi.copyData(bi.getRaster().createCompatibleWritableRaster());
	    return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
	}
	
	/**
	 * @return l'image de la caseDecore
	 */
	public BufferedImage getImageCaseDecore(){
		return this.getCaseDecore().getImage();
	}
	
	/**
	 * @return la caseDecore
	 */
	public Case getCaseDecore(){
		return this.caseDecore;
	}



}
