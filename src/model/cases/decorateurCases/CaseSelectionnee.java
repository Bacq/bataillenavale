package model.cases.decorateurCases;

import java.awt.image.BufferedImage;

import model.cases.Case;
import model.fabriques.FabriqueImage;

public class CaseSelectionnee extends DecorateurCase {

	
	private BufferedImage imageSelectionnee;
	
	public CaseSelectionnee(Case ancienneCase) {
		super(ancienneCase);
		
		BufferedImage imageBateau = this.deepCopy(this.getImageCaseDecore());
		this.imageSelectionnee = this.superposerImages(imageBateau, FabriqueImage.getImageSelectionee());		
	}

	@Override
	public BufferedImage getImage(){
		return this.imageSelectionnee;
	}
		
	@Override
	public String toCSV() {
		//toCSV d'un bateau
		return this.caseDecore.toCSV();
	}

}
