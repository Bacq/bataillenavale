package model.cases.decorateurCases;

import java.awt.image.BufferedImage;

import model.bateaux.Bateau;
import model.cases.Case;

public class CaseBateau extends DecorateurCase{

	private Bateau bateau; 
	private BufferedImage imageBateau; //a superposer aux autres images des objets decore
	
	public CaseBateau(Case caseAdecorer, Bateau b, BufferedImage imgASuperpose) {
		super(caseAdecorer);
		this.bateau = b;
		this.imageBateau = this.superposerImages(this.getImageCaseDecore(), imgASuperpose);
	}
	
	public Bateau getBateau(){
		return this.bateau;
	}
	
	public boolean isDetruit(){
		return this.getBateau().isDetruit();
	}
	
	public void degat(){
		this.getBateau().degat();
	}
	
	@Override
	public boolean touche(){
		return true;
	}
	
	@Override
	public BufferedImage getImage(){
		return this.imageBateau;
	}
	
	public void tire(){
		this.bateau.decrementerMunition();
	}
	
	@Override
	public String toCSV() {
		return ("bateau");
	}
	
}
