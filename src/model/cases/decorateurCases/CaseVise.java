package model.cases.decorateurCases;

import java.awt.image.BufferedImage;

import model.cases.Case;

public class CaseVise extends DecorateurCase{
	
	private boolean touche;
	
	BufferedImage imageVise;
	
	public CaseVise(Case c, BufferedImage imgASuperpose) {
		super(c);
		this.imageVise = this.superposerImages(this.getImageCaseDecore(), imgASuperpose);
	}
	
	@Override
	public boolean isClickable(){
		return false;
	}

	@Override
	public BufferedImage getImage(){
		return this.imageVise;
	}
	
	@Override
	public String toCSV() {
		return ("-vise-");
	}
	
}
