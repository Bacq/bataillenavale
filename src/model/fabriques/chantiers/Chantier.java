package model.fabriques.chantiers;

import model.bateaux.Bateau;

public abstract class Chantier {
	
	public final static String CHANTIER17E = "Chantier17e";
	public final static String CHANTIER20E = "Chantier20e";

	
	public abstract Bateau construireBateau4Case();
	
	public abstract Bateau construireBateau3Case();
	
	public abstract Bateau construireBateau2Case();
	
	public abstract String toCSV();
	
	public abstract String toString();
	
	/**
	 * @param taille
	 * @return un bateau selon sont nombres de cases
	 */
	public Bateau getBateauSelonTaille(int taille){
		Bateau bateau = null;
		switch(taille){
		case Bateau.BATEAU2:
			bateau = this.construireBateau2Case();
			break;
		case Bateau.BATEAU3:
			bateau = this.construireBateau3Case();
			break;
		case Bateau.BATEAU4:
			bateau = this.construireBateau4Case();
			break;		
		}
		return bateau;
	}
	
	
}
