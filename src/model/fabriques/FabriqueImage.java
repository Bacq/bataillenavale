package model.fabriques;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import model.fabriques.chantiers.Chantier;

import utiles.RotationBufferedImage;


public class FabriqueImage {
	
	private static String URL_EAU = "ressources/img/eau.png";
	private static String URL_TOUCHE = "ressources/img/touche.png";
	private static String URL_RATE = "ressources/img/rate.png";
	private static String URL_SELECTIONNEE = "ressources/img/selection.png";

	
	private static String URL_BATEAU2_20EP1 = "ressources/img/bateaux/2_20e1.png";
	private static String URL_BATEAU2_20EP2 = "ressources/img/bateaux/2_20e2.png";

	private static String URL_BATEAU3_20EP1 = "ressources/img/bateaux/3_20e1.png";
	private static String URL_BATEAU3_20EP2 = "ressources/img/bateaux/3_20e2.png";
	private static String URL_BATEAU3_20EP3 = "ressources/img/bateaux/3_20e3.png";

	private static String URL_BATEAU4_20EP1 = "ressources/img/bateaux/4_20e1.png";
	private static String URL_BATEAU4_20EP2 = "ressources/img/bateaux/4_20e2.png";
	private static String URL_BATEAU4_20EP3 = "ressources/img/bateaux/4_20e3.png";
	private static String URL_BATEAU4_20EP4 = "ressources/img/bateaux/4_20e4.png";
	
	private static String URL_BATEAU2_17EP1 = "ressources/img/bateaux/2_17e1.png";
	private static String URL_BATEAU2_17EP2 = "ressources/img/bateaux/2_17e2.png";

	private static String URL_BATEAU3_17EP1 = "ressources/img/bateaux/3_17e1.png";
	private static String URL_BATEAU3_17EP2 = "ressources/img/bateaux/3_17e2.png";
	private static String URL_BATEAU3_17EP3 = "ressources/img/bateaux/3_17e3.png";

	private static String URL_BATEAU4_17EP1 = "ressources/img/bateaux/4_17e1.png";
	private static String URL_BATEAU4_17EP2 = "ressources/img/bateaux/4_17e2.png";
	private static String URL_BATEAU4_17EP3 = "ressources/img/bateaux/4_17e3.png";
	private static String URL_BATEAU4_17EP4 = "ressources/img/bateaux/4_17e4.png";

	
	/********************************* IMAGE EAU *********************************/
		
	public static ImageIcon getImageIconEau(){
		ImageIcon eau = new ImageIcon(getImageEau());
		return eau;
	}
	
	public static BufferedImage getImageEau(){
		BufferedImage eau = null;
		try {
			eau = ImageIO.read(new File(URL_EAU));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return eau;
	}

	/********************************* IMAGE TOUCHE *********************************/

	
	public static BufferedImage getImageTouche(){
		BufferedImage bateau = null;
		try {
			bateau = ImageIO.read(new File(URL_TOUCHE));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bateau;
	}
	
	/********************************* IMAGE RATE *********************************/

	
	public static BufferedImage getImageRate(){
		BufferedImage bateau = null;
		try {
			bateau = ImageIO.read(new File(URL_RATE));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bateau;
	}

	
	
	/********************************* IMAGE SELECTIONNEE *********************************/
	
	
	public static BufferedImage getImageSelectionee() {
		BufferedImage bateau = null;
		try {
			bateau = ImageIO.read(new File(URL_SELECTIONNEE));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bateau;
	}

	/********************************* IMAGE BATEAU *********************************/

	
	public static BufferedImage getImageBateau(int position, int taille, int[] direction, String epoque){
		BufferedImage bateau = null;
		switch (epoque){
		case Chantier.CHANTIER17E:
			bateau = getImageBateau17e(position, taille, direction);
			break;
		case Chantier.CHANTIER20E:
			bateau = getImageBateau20e(position, taille, direction);
			break;
		}
		return bateau;
	}
	
	public static BufferedImage getImageBateau20e(int position, int taille, int[] direction){
		BufferedImage bateau = null;
		switch(taille){
		case 2:
			switch(position){
			case 0:
				try {
					bateau = ImageIO.read(new File(URL_BATEAU2_20EP1));
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case 1:
				try {
					bateau = ImageIO.read(new File(URL_BATEAU2_20EP2));
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			}
			break;
		case 3:
			switch(position){
			case 0:
				try {
					bateau = ImageIO.read(new File(URL_BATEAU3_20EP1));
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case 1:
				try {
					bateau = ImageIO.read(new File(URL_BATEAU3_20EP2));
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case 2:
				try {
					bateau = ImageIO.read(new File(URL_BATEAU3_20EP3));
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			}
			break;
		case 4:
			switch(position){
			case 0:
				try {
					bateau = ImageIO.read(new File(URL_BATEAU4_20EP1));
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case 1:
				try {
					bateau = ImageIO.read(new File(URL_BATEAU4_20EP2));
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case 2:
				try {
					bateau = ImageIO.read(new File(URL_BATEAU4_20EP3));
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case 3:
				try {
					bateau = ImageIO.read(new File(URL_BATEAU4_20EP4));
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			}
			break;
		}
		return RotationBufferedImage.rotationSelonDirection(bateau, direction);
	}
	
	public static BufferedImage getImageBateau17e(int position, int taille, int[] direction){
		BufferedImage bateau = null;
		switch(taille){
		case 2:
			switch(position){
			case 0:
				try {
					bateau = ImageIO.read(new File(URL_BATEAU2_17EP1));
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case 1:
				try {
					bateau = ImageIO.read(new File(URL_BATEAU2_17EP2));
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			}
			break;
		case 3:
			switch(position){
			case 0:
				try {
					bateau = ImageIO.read(new File(URL_BATEAU3_17EP1));
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case 1:
				try {
					bateau = ImageIO.read(new File(URL_BATEAU3_17EP2));
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case 2:
				try {
					bateau = ImageIO.read(new File(URL_BATEAU3_17EP3));
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			}
			break;
		case 4:
			switch(position){
			case 0:
				try {
					bateau = ImageIO.read(new File(URL_BATEAU4_17EP1));
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case 1:
				try {
					bateau = ImageIO.read(new File(URL_BATEAU4_17EP2));
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case 2:
				try {
					bateau = ImageIO.read(new File(URL_BATEAU4_17EP3));
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case 3:
				try {
					bateau = ImageIO.read(new File(URL_BATEAU4_17EP4));
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			}
			break;
		}
		return RotationBufferedImage.rotationSelonDirection(bateau, direction);
	}
	
}
