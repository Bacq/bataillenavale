package vues;

import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import model.BatailleNavale;

public class VuePlateaux extends JPanel implements Observer {

	private static final long serialVersionUID = 1L;
	
	private BatailleNavale model;
	
	public VuePlateaux(BatailleNavale aModel){
		this.model = aModel;
		
		this.setLayout(new GridLayout(1, 2));
		
		VuePlateauJoueur vpj = new VuePlateauJoueur(model);
		this.add(vpj);
		
		VuePlateauTir vpt = new VuePlateauTir(model);
		this.add(vpt);
		
	}
	
	@Override
	public void update(Observable arg0, Object arg1) {
		//Ne fais rien
	}


}
