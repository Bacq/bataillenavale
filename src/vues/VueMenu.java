package vues;

import iATactique.TactiqueAleatoire;
import iATactique.TactiqueCroix;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ButtonGroup;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;

import model.BatailleNavale;

public class VueMenu extends JMenuBar implements Observer {

	private static final long serialVersionUID = 1L;

	private BatailleNavale model;

	private JMenu menuFichier;
	private JMenuItem itemSauvegarde;
	private JMenuItem itemOuvrir;
	private JRadioButtonMenuItem tacticAleatoire;
	private JRadioButtonMenuItem tacticCroix;	
	
	private static final String VISE = "-vise-";
	
	
	public VueMenu(BatailleNavale aModel){
		this.model = aModel;
		
		/*********************************** MENU FICHIER ***********************************/
 		
		menuFichier = new JMenu("Fichier");
		this.add(menuFichier);
		
		itemSauvegarde = new JMenuItem("Sauvegarder Partie");
		itemSauvegarde.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				JFileChooser jfc = new JFileChooser();
				//jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				jfc.setCurrentDirectory(new File(System.getProperty("user.dir")));
				int retour = jfc.showOpenDialog(null);
				if(retour == JFileChooser.APPROVE_OPTION){
					try{
						File ff = jfc.getSelectedFile(); // definir l'arborescence
						ff.createNewFile();
						FileWriter ffw=new FileWriter(ff);
						ffw.write(model.toCSV());  // ecrire une ligne dans le fichier resultat.txt
						ffw.close(); // fermer le fichier a la fin des traitements
					} catch (Exception ee) {}
				}
				
			}
		});
		menuFichier.add(itemSauvegarde);
		
		
		itemOuvrir = new JMenuItem("Ouvrir Partie");
		itemOuvrir.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				JFileChooser jfc = new JFileChooser();
				//Retire le filtre qui accepte tous les fichiers
				jfc.removeChoosableFileFilter(jfc.getAcceptAllFileFilter());
				//Ajoute le filtre qui accepte les fichiers voulu
//				jfc.addChoosableFileFilter(new FiltreFileChooser());
				jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
				jfc.setMultiSelectionEnabled(true);
				jfc.setCurrentDirectory(new File(System.getProperty("user.dir")));
				int retour = jfc.showOpenDialog(null);
				if(retour == JFileChooser.APPROVE_OPTION){
					System.out.println(jfc.getSelectedFile().getName());
					try{
						File ff = jfc.getSelectedFile(); // définir l'arborescence
						InputStream flux = new FileInputStream(ff); 
						InputStreamReader lecture = new InputStreamReader(flux);
						BufferedReader buff = new BufferedReader(lecture);
						String ligne = buff.readLine();
						String[] modelInfo = ligne.split(",");
						model.chargerBatailleNavale(modelInfo[2], modelInfo[3], modelInfo[5]); 
						String joueur;
						String[] bateauLignePos, bateauxLigne, casePos1, casePos2, ligneTEMPORAIRE;
						int nbrBateau;
						while ((ligne=buff.readLine())!=null){
							joueur = ligne;
							//On recupere le nombre de bateau
							nbrBateau = Integer.parseInt(buff.readLine());
							//On recupere chaque ligne de bateaux
							for(int i = 0; i<nbrBateau; i++) {
								//On recupere la ligne du bateau et ses coordonees
								bateauLignePos = buff.readLine().split(";");
								//On recupere les donnees du bateau
								bateauxLigne = bateauLignePos[0].split(",");
								
								casePos1 = bateauLignePos[1].split(",");
								casePos2 = bateauLignePos[Integer.parseInt(bateauxLigne[2])].split(",");
								
								//On cree un bateau correspondant
								if (joueur.equals("joueur1")){
									model.getPlateauJoueurJoueur().placementBateau(Integer.parseInt(casePos1[0]), Integer.parseInt(casePos1[1]), Integer.parseInt(casePos2[0]), Integer.parseInt(casePos2[1]));
									model.getPlateauJoueurJoueur().setNbrMunitionDernierBateau(Integer.parseInt(bateauxLigne[1]));
								}else{
									model.getPlateauJoueurOrdinateur().placementBateau(Integer.parseInt(casePos1[0]), Integer.parseInt(casePos1[1]), Integer.parseInt(casePos2[0]), Integer.parseInt(casePos2[1]));
									model.getPlateauJoueurOrdinateur().setNbrMunitionDernierBateau(Integer.parseInt(bateauxLigne[1]));
								}
								//TODO enlever munitions déjà utilisées 
							}
							//si la case est une case visée, on la décore
							for(int i = 0; i < 10; i++) {
								ligne = buff.readLine();
								ligneTEMPORAIRE = ligne.split(",");
								for(int j = 0; j < 10; j++) {
									switch (ligneTEMPORAIRE[j]) {
										case VISE :
											if (joueur.equals("joueur1")){
												model.CaseVise(model.getPlateauTirOrdinateur(), model.getPlateauJoueurJoueur(), i, j);
											}else{
												model.CaseVise(model.getPlateauTirJoueur(), model.getPlateauJoueurOrdinateur(), i, j);
											}
											break;
										default :
											break;
									}
								}
							}
						}
						model.chargerTactique(modelInfo[4]);
						buff.close(); 
					} catch (Exception ex) {
						ex.printStackTrace(); 
			            System.out.println(ex); 
					}
				}
				model.update();
			}
		});
		menuFichier.add(itemOuvrir);	
		
		
		
		
		
		/*********************************** MENU STRATEGIES ****************************/
		
		menuFichier = new JMenu("Strategies");
		this.add(menuFichier);
		
		//Button Aleatoire
		tacticAleatoire = new JRadioButtonMenuItem("Aleatoire");
		tacticAleatoire.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				model.changeIa(new TactiqueAleatoire(model.getPlateauTirOrdinateur()));
			}
		});
		menuFichier.add(tacticAleatoire);
		//On met le radioButton tacticAleatoire coché en premier
		this.tacticAleatoire.setSelected(true);
		
		//Button croix
		tacticCroix = new JRadioButtonMenuItem("Croix");
		tacticCroix.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				model.changeIa(new TactiqueCroix());
			}
		});
		menuFichier.add(tacticCroix);
		
		//On ajoute les deux RadioButton a un ButtonGroup pour faire un choix exclusive
		ButtonGroup grp = new ButtonGroup();
		grp.add(tacticAleatoire);		
		grp.add(tacticCroix);

	
		this.model.addObserver(this);
		
		this.model.update(); //TODO TEMPORAIRE
	}
	
	public void update(Observable o, Object arg) {
		
	}

}

