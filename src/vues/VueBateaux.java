package vues;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import model.BatailleNavale;
import model.bateaux.Bateau;
import model.fabriques.chantiers.Chantier;
import model.plateaux.Plateau;

public class VueBateaux extends JPanel implements Observer {

	private static final long serialVersionUID = 1L;

	private BatailleNavale model;	
	
	
	//Partie gauche
	private JLabel[] nbBateaux;
	private JLabel munitionsTotal;
	private JLabel munitionsBateau;
	private int nbCasesGridLayout;

	private JPanel panelDroite;
	private JButton lancerPartie;
	private JButton recommencer;
	
	private JButton boutonPlacer;
	
	private JRadioButton choixEpoque20e;
	private JRadioButton choixEpoque17e;

	private JRadioButton choixModeNormale;
	private JRadioButton choixModeMunitions;

	
	public VueBateaux(BatailleNavale m){
		this.model = m;
		this.setLayout(new GridLayout(1, 2));	
		
		/*************************** PARTIE GAUCHE ***************************/
		
		this.nbCasesGridLayout = Bateau.NB_CASES_MAX - Bateau.NB_CASES_MIN + 1; //+1 car la difference oubli une case
		JPanel panelBateaux = new JPanel();
		panelBateaux.setLayout(new GridLayout(nbCasesGridLayout, 1));
		
		//Label pour afficher le nombre de bateaux restant
		this.nbBateaux = new JLabel[nbCasesGridLayout];
		for(int i=0; i<this.nbCasesGridLayout; i++){
			this.nbBateaux[i] = new JLabel();
			panelBateaux.add(this.nbBateaux[i]);
		}
		
		JPanel panelMunitions = new JPanel();
		panelMunitions.setLayout(new GridLayout(2, 1));

		//Label pour les munitions Total
		this.munitionsTotal = new JLabel();		
		panelMunitions.add(munitionsTotal);
		//Mabel pour les munitions du bateau selectionnees
		this.munitionsBateau = new JLabel();		
		panelMunitions.add(munitionsBateau);
		
		//Cree un panel pour regrouper les informations de la partie
		JPanel panelInfo = new JPanel();
		panelInfo.setLayout(new GridLayout(1,2));
		panelInfo.add(panelBateaux);
		panelInfo.add(panelMunitions);
		
		//Ajoute le panelInfo a la fnetre
		this.add(panelInfo);

		
		/*************************** PARTIE DROITE ***************************/

		this.panelDroite = new JPanel();
		this.add(this.panelDroite);
		
		//Bouton pour lancer la partie
		this.lancerPartie = new JButton("Lancer");
		this.lancerPartie.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				model.lancerPartie();
			}
		});
		
		//Bouton pour recommencer la partie
		this.recommencer = new JButton("Recommencer");
		this.recommencer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				model.init();
				model.update();
			}
		});

		//Radio button choix epoque
		this.choixEpoque17e = new JRadioButton("17e siecle");
		this.choixEpoque20e = new JRadioButton("20e siecle");
		final ButtonGroup groupeEpoque = new ButtonGroup();
		groupeEpoque.add(this.choixEpoque17e);
		groupeEpoque.add(this.choixEpoque20e);
		
		//Radio button choix mode
		this.choixModeNormale = new JRadioButton("Normal");
		this.choixModeMunitions = new JRadioButton("Munitions");
		final ButtonGroup groupeMode = new ButtonGroup();
		groupeMode.add(this.choixModeNormale);
		groupeMode.add(this.choixModeMunitions);
		
		this.boutonPlacer = new JButton("Placer Bateaux");
		this.boutonPlacer.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//Si c'est epoque 17e qui est selectionnée
				if(choixEpoque17e.isSelected() && !choixEpoque20e.isSelected()){
					//Si c'est mode normal qui est selectionnée
					if(choixModeNormale.isSelected() && !choixModeMunitions.isSelected()){
						model.setModeNormal();
						model.setChantier(Chantier.CHANTIER17E);
						model.setEtatPlacement();
					}
					//Si c'est mode munitions qui est selectionnée
					if(!choixModeNormale.isSelected() && choixModeMunitions.isSelected()){
						model.setModeMunitions();
						model.setChantier(Chantier.CHANTIER17E);
						model.setEtatPlacement();
					}
				}
				//Si c'est epoque 20e qui est selectionnée
				if(!choixEpoque17e.isSelected() && choixEpoque20e.isSelected()){
					//Si c'est mode normal qui est selectionnée
					if(choixModeNormale.isSelected() && !choixModeMunitions.isSelected()){
						model.setModeNormal();
						model.setChantier(Chantier.CHANTIER20E);
						model.setEtatPlacement();
					}
					//Si c'est mode munitions qui est selectionnée
					if(!choixModeNormale.isSelected() && choixModeMunitions.isSelected()){
						model.setModeMunitions();
						model.setChantier(Chantier.CHANTIER20E);
						model.setEtatPlacement();
					}
				}
			}
		});
		
		
		//Remplit les bateaux restant et les munitions
		this.updtateText();
		this.updatePartieDroite();
		this.model.addObserver(this);
	}
	
	
	
	public void updtateText(){
		Plateau plateau = this.model.getPlateauJoueurJoueur();
		for(int i=0; i<this.nbCasesGridLayout; i++){
			int nbCases = i + Bateau.NB_CASES_MIN;
			this.nbBateaux[i].setText("Bateau(x) " + nbCases + " cases: " + plateau.getNbBateaux(nbCases) + "/" + plateau.getnbBateauMax(nbCases));
		}
		
		//update les labels de munitions
		if(model.isModeMunitions()){
			this.munitionsTotal.setText("Munition(s) totales: " + plateau.getNbMunitions());
			
			if(this.model.bateauSelectionneNonNull()){
				this.munitionsBateau.setText("Munition(s) du bateau : " + this.model.getMunitionsBateauSelectionne());
			}
			else{
				this.munitionsBateau.setText("Munition(s) du bateau : NA");
			}
		}
	}
	
	public void updatePartieDroite(){
		//Retire tous les elements du panel
		this.panelDroite.removeAll();

		if(this.model.isPlacementBateau()){
			this.panelDroite.setLayout(new GridLayout());
			this.panelDroite.add(this.lancerPartie);
		}
		
		if(this.model.isGameRunning()){
			this.panelDroite.setLayout(new GridLayout());
			this.panelDroite.add(this.recommencer);
		}
		
		
		if(this.model.isConfiguration()){
			this.panelDroite.setLayout(new GridLayout(1,2));
			
			JPanel panelRadioBuuttons = new JPanel();
			panelRadioBuuttons.setLayout(new GridLayout(2,2));
			panelRadioBuuttons.add(choixEpoque17e);
			panelRadioBuuttons.add(choixModeNormale);
			panelRadioBuuttons.add(choixEpoque20e);
			panelRadioBuuttons.add(choixModeMunitions);
			
			this.panelDroite.add(panelRadioBuuttons);
			this.panelDroite.add(this.boutonPlacer);
		}
		
		panelDroite.revalidate();
		panelDroite.repaint();
	}
	
	@Override
	public void update(Observable arg0, Object arg1) {
		this.updtateText();
		this.updatePartieDroite();
		if(model.isEnd()){
			JOptionPane.showMessageDialog(this, "Vous avez " + this.model.getGagner() + " !");
		}
	}

}
