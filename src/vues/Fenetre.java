package vues;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;

import model.BatailleNavale;
import model.cases.Case;
import model.plateaux.Plateau;

public class Fenetre extends JFrame {

	private static final long serialVersionUID = 1L;	
	private static int AJOUT_COL = 50;
	private static int AJOUT_LIG = 100;
	
	public Fenetre(){
		BatailleNavale model = new BatailleNavale();
				
		VuePlateaux vp = new VuePlateaux(model);
		this.add(vp, BorderLayout.CENTER);
		
		VueBateaux vb = new VueBateaux(model);
		this.add(vb, BorderLayout.SOUTH);
		
		this.setJMenuBar(new VueMenu(model));
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//*2 pour les colonnes, car il y a deux plateaux
		this.setPreferredSize(new Dimension(Plateau.getNB_LIG()*Case.TAILLE_CASE*2 + AJOUT_COL, Plateau.getNB_COL()*Case.TAILLE_CASE + AJOUT_LIG));
		setVisible(true) ;
		this.pack() ;
	}

	public static void main(String[] args){
		new Fenetre();
	}
}
