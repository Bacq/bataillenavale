package vues;

import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import controleurs.ControleurTir;


import model.BatailleNavale;
import model.cases.Case;
import model.plateaux.Plateau;

public class VuePlateauTir extends JPanel implements Observer {

	private static final long serialVersionUID = 1L;
	
	private JButton[][] plateauBoutons;
	private BatailleNavale model;
	
	public VuePlateauTir(BatailleNavale aModel){
		this.model = aModel;
		
		this.setLayout(new GridLayout(Plateau.getNB_LIG(), Plateau.getNB_COL()));
		
		this.plateauBoutons = new JButton[Plateau.getNB_LIG()][Plateau.getNB_COL()];
		this.initPlateauBoutons();

		
		this.model.addObserver(this);
	}
	
	public void initPlateauBoutons(){
		for(int lig=0; lig<Plateau.getNB_LIG(); lig++){
			for(int col=0; col<Plateau.getNB_COL(); col++){
				Case caseCourante = model.getPlateauTirJoueur().getCase(lig, col);
				this.plateauBoutons[lig][col] = new JButton(new ImageIcon(caseCourante.getImage()));
				this.plateauBoutons[lig][col].addActionListener(new ControleurTir(this.model, lig, col));
				this.add(this.plateauBoutons[lig][col]);
			}
		}
	}
	
	public void updatePlateaux(){
		for(int lig=0; lig<Plateau.getNB_LIG(); lig++){
			for(int col=0; col<Plateau.getNB_COL(); col++){
				Case caseCourante = model.getPlateauTirJoueur().getCase(lig, col);
				this.plateauBoutons[lig][col].setIcon(new ImageIcon(caseCourante.getImage()));
			}
		}
	}
	
	@Override
	public void update(Observable arg0, Object arg1) {
		this.updatePlateaux();
	}

}
























